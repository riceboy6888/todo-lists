//
//  AllListsViewController.swift
//  Checklists
//
//  Created by Pascal Huynh on 05/11/14.
//  Copyright (c) 2014 Beyowi. All rights reserved.
//

import UIKit

class AllListsViewController: UITableViewController, ListDetailViewControllerDelegate, UINavigationControllerDelegate {

  var dataModel: DataModel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = false
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem()
  }
  
  override func viewDidAppear(animated: Bool) {
    println("### viewDidAppear")
    super.viewDidAppear(animated)
    
    // The very first time AllListsViewController's screen becomes visible you don't want the
    // "willShowViewController" delegate method to be called yet, as that would always overwrite
    // the old value of "ChecklistIndex" with -1, before you’ve had a chance to restore the old screen.
    
    // By waiting to register AllListsViewController as the navigation controller delegate until it is 
    // visible, you avoid this problem. viewDidAppear() is the ideal place for that, so it makes sense 
    // to do it from that method.
    navigationController?.delegate = self

    let index = dataModel.indexOfSelectedChecklist
    if index >= 0 && index < dataModel.lists.count {
      let checklist = dataModel.lists[index]
      performSegueWithIdentifier("ShowChecklist", sender: checklist)
    }
  }
  
  // That will cause tableView(cellForRowAtIndexPath) to be called again for every visible row.
  // That should update the "remaining" 
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    tableView.reloadData()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // MARK: - Table view data source
  
  override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return dataModel.lists.count
  }
  
  override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cellIdentifier = "Cell"
    var cell: UITableViewCell! = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? UITableViewCell
    
    // Contruct a new cell with an identifier if no cell is found
    if cell == nil {
      cell = UITableViewCell(style: .Subtitle, reuseIdentifier: cellIdentifier)
    }
    
    let checklist = dataModel.lists[indexPath.row]
    cell.textLabel.text = checklist.name
    cell.accessoryType = .DetailDisclosureButton
    
    let count = checklist.countUncheckItems()
    if checklist.items.count == 0 {
      cell.detailTextLabel!.text = "(No Items)"      
    } else if count == 0 {
      cell.detailTextLabel!.text = "All Done!"
    } else {
      cell.detailTextLabel!.text = "\(checklist.countUncheckItems()) Remaining"
    }

    cell.imageView.image = UIImage(named: checklist.iconName)
    return cell
  }
  
  override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    // Save the latest row clicked in User Preference
    dataModel.indexOfSelectedChecklist = indexPath.row
    
    let checklist = dataModel.lists[indexPath.row]
    // Since the segue is performed manually, we can put anything in the sender
    // But need to override 'prepareForSegue'
    performSegueWithIdentifier("ShowChecklist", sender: checklist)
  }
  
  // 'viewDidLoad' of the destination controller is called right after 'prepareForSegue'
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if segue.identifier == "ShowChecklist" {
      let controller = segue.destinationViewController as ChecklistViewController
      controller.checklist = sender as Checklist
    } else if segue.identifier == "AddChecklist" {
      let navigationController = segue.destinationViewController as UINavigationController
      let controller = navigationController.topViewController as ListDetailViewController
      controller.delegate = self
      controller.checklistToEdit = nil
    }
  }
  
  // Allow user to delete checklists
  override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    dataModel.lists.removeAtIndex(indexPath.row)
    let indexPaths = [indexPath]
    tableView.deleteRowsAtIndexPaths(indexPaths, withRowAnimation: .Automatic)
  }
  
  // Another way (other than the segue) to trigger the edit list view
  override func tableView(tableView: UITableView, accessoryButtonTappedForRowWithIndexPath indexPath: NSIndexPath) {
    let navigationController = storyboard!.instantiateViewControllerWithIdentifier("ListNavigationController") as UINavigationController
    let controller = navigationController.topViewController as ListDetailViewController
    controller.delegate = self
    
    let checklist = dataModel.lists[indexPath.row]
    controller.checklistToEdit = checklist
    
    presentViewController(navigationController, animated: true, completion: nil)
  }
  
  func listDetailViewControllerDidCancel(controller: ListDetailViewController) {
    dismissViewControllerAnimated(true, completion: nil)
  }
  
  func listDetailViewController(controller: ListDetailViewController, didFinishAddingChecklist checklist: Checklist) {
    dataModel.lists.append(checklist)
    dataModel.sortChecklists()
    tableView.reloadData()
    dismissViewControllerAnimated(true, completion: nil)
  }
  
  func listDetailViewController(controller: ListDetailViewController, didFinishEditingChecklist checklist: Checklist) {
    dataModel.sortChecklists()
    tableView.reloadData()
    dismissViewControllerAnimated(true, completion: nil)
  }
  
  // UINavigationController Delegate Method
  
  // This method is called whenever the navigation controller will slide to a new screen
  func navigationController(navigationController: UINavigationController, willShowViewController viewController: UIViewController, animated: Bool) {
    println("### willShowViewController")
    if viewController === self {
      dataModel.indexOfSelectedChecklist = -1
    }
  }
}
