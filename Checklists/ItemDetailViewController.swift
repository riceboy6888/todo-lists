//
//  ItemDetailViewController.swift
//  Checklists
//
//  Created by Pascal Huynh on 03/11/14.
//  Copyright (c) 2014 Beyowi. All rights reserved.
//

import UIKit

// Delegate - Protocols pattern
/* This defines the ItemDetailViewControllerDelegate protocol. 
   You should recognize the lines inside the protocol { ... } block as method declarations, 
   but unlike other methods they don’t have any source code in them. The protocol just lists the names of the methods.
*/
protocol ItemDetailViewControllerDelegate: class {
  func itemDetailViewControllerDidCancel(controller: ItemDetailViewController)
  func itemDetailViewController(controller: ItemDetailViewController, didFinishAddingItem item: ChecklistItem)
  func itemDetailViewController(controller: ItemDetailViewController, didFinishEditingItem item: ChecklistItem)
}

class ItemDetailViewController: UITableViewController, UITextFieldDelegate {

  @IBOutlet weak var doneBarButton: UIBarButtonItem!
  @IBOutlet weak var textField: UITextField!
  @IBOutlet weak var shouldRemindSwitch: UISwitch!
  @IBOutlet weak var dueDateLabel: UILabel!
  // Delegate are usually declared as weak and optional
  weak var delegate: ItemDetailViewControllerDelegate?
  var itemToEdit: ChecklistItem?
  var dueDate = NSDate()
  var datePickerVisible = false
  
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.rowHeight = 44
    
    // Here we unwrap first the optional
    if let item = itemToEdit {
      // Change the title of the view. Title is a built-in property of the current view that changes the text in the navigation bar.
      title = "Edit Item"
      textField.text = item.text
      doneBarButton.enabled = true
      shouldRemindSwitch.on = item.shouldRemind
      dueDate = item.dueDate
    }
    updateDueDateLabel()
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    // Force the keyboard to show up
    textField.becomeFirstResponder()
  }
  
  @IBAction func cancel() {
    delegate?.itemDetailViewControllerDidCancel(self)
  }
  @IBAction func done() {
    if let item = itemToEdit {
      item.text = textField.text
      item.shouldRemind = shouldRemindSwitch.on
      item.dueDate = dueDate
      item.scheduleNotification()
      delegate?.itemDetailViewController(self, didFinishEditingItem: item)
    } else {
      let item = ChecklistItem()
      item.text = textField.text
      item.checked = false
      item.shouldRemind = shouldRemindSwitch.on
      item.dueDate = dueDate
      item.scheduleNotification()
      delegate?.itemDetailViewController(self, didFinishAddingItem: item)
    }
  }
  
  @IBAction func shouldRemindToggled(switchControl: UISwitch) {
    textField.resignFirstResponder()
    if switchControl.on {
      let notificationSettings = UIUserNotificationSettings(forTypes: .Alert | .Sound, categories: nil)
      UIApplication.sharedApplication().registerUserNotificationSettings(notificationSettings)
    }
  }
  
  override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    if indexPath.section == 1 && indexPath.row == 2 {
      // Check if there is an existing date picker cell
      var cell: UITableViewCell! = tableView.dequeueReusableCellWithIdentifier("DatePickerCell") as? UITableViewCell

      if cell == nil {
        cell = UITableViewCell(style: .Default, reuseIdentifier: "DatePickerCell")
        cell.selectionStyle = .None
        
        // Create a new UIDatePicker component
        let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: 320, height: 216))
        datePicker.tag = 100
        cell.contentView.addSubview(datePicker)
        
        // <=> action methods connection from interface builder
        datePicker.addTarget(self, action: Selector("dateChanged:"), forControlEvents: .ValueChanged)
      }
      return cell
    } else {
      // That makes sure that the other static cells still work
      return super.tableView(tableView, cellForRowAtIndexPath: indexPath)
    }
  }
  
  override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if section == 1 && datePickerVisible {
      return 3
    } else {
      // That makes sure that the other static cells still work
      return super.tableView(tableView, numberOfRowsInSection: section)
    }
  }
  
  override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    if indexPath.section == 1 && indexPath.row == 2 {
      // The UIDatePicker component is 216 points tall, plus 1 point for the separator line, makes for a total row height of 217 points.
      return 217
    } else {
      return super.tableView(tableView, heightForRowAtIndexPath: indexPath)
    }
  }
  
  override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    tableView.deselectRowAtIndexPath(indexPath, animated: true)
    textField.resignFirstResponder()
    
    if indexPath.section == 1 && indexPath.row == 1 {
      if !datePickerVisible {
        showDatePicker()
      } else {
        hideDatePicker()
      }
    }
  }
  
  // Make the Due Date row tappable
  override func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
    if indexPath.section == 1 && indexPath.row == 1 {
      return indexPath
    } else {
      return nil
    }
  }
  
  // The reason the app crashed on this method was that the standard data source doesn’t know anything about the cell at row 2 in section 1 
  // (the one with the date picker), because that cell isn’t part of the table view’s design in the storyboard.
  // So after inserting the new date picker cell the data source gets confused and it crashes the app. 
  // To fix this, you have to trick the data source into believing there really are three rows in that section when the date picker is visible.
  override func tableView(tableView: UITableView, var indentationLevelForRowAtIndexPath indexPath: NSIndexPath) -> Int {
    if indexPath.section == 1 && indexPath.row == 2 {
      indexPath = NSIndexPath(forRow: 0, inSection: indexPath.section)
    }
    return super.tableView(tableView, indentationLevelForRowAtIndexPath: indexPath)
  }
  
  func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
    /* The textField(shouldChangeCharactersInRange) delegate method doesn’t give you the new text,
       only which part of the text should be replaced (the range) and the text it should be replaced with (the replacement string)
      
       You need to calculate what the new text will be by taking the text field’s text and doing the replacement yourself. 
       This gives you a new string object that you store in the newText constant.
    */
    let oldText: NSString = textField.text
    let newText: NSString = oldText.stringByReplacingCharactersInRange(range, withString: string)
    
    doneBarButton.enabled = (newText.length > 0)
    return true
  }
  
  // Hide the date picker when the user taps inside the text field
  func textFieldDidBeginEditing(textField: UITextField) {
    hideDatePicker()
  }
  
  // Convert date to text
  func updateDueDateLabel() {
    let formatter = NSDateFormatter()
    formatter.dateStyle = .MediumStyle
    formatter.timeStyle = .ShortStyle
    dueDateLabel.text = formatter.stringFromDate(dueDate)
  }
  
  func showDatePicker() {
    // This sets the new instance variable to true, and tells the table view to insert a new row below the Due Date cell.
    // This new row will contain the UIDatePicker component.
    datePickerVisible = true
    
    let indexPathDateRow = NSIndexPath(forRow: 1, inSection: 1)
    let indexPathDatePicker = NSIndexPath(forRow: 2, inSection: 1)
    
    if let dateCell = tableView.cellForRowAtIndexPath(indexPathDateRow) {
      dateCell.detailTextLabel!.textColor = dateCell.detailTextLabel!.tintColor
    }
    // If there is many updates in the same time in a tableview (insert and reload), add beginUpdates and endUpdates
    tableView.beginUpdates()
    tableView.insertRowsAtIndexPaths([indexPathDatePicker], withRowAnimation: .Fade)
    tableView.reloadRowsAtIndexPaths([indexPathDateRow], withRowAnimation: .None)
    tableView.endUpdates()
    
    // Locate the UIDatePicker component in the new cell and gives it the proper date
    if let pickerCell = tableView.cellForRowAtIndexPath(indexPathDatePicker) {
      let datePicker = pickerCell.viewWithTag(100) as UIDatePicker
      datePicker.setDate(dueDate, animated: false)
    }
  }
  
  func hideDatePicker() {
    if datePickerVisible {
      datePickerVisible = false
      let indexPathDateRow = NSIndexPath(forRow: 1, inSection: 1)
      let indexPathDatePicker = NSIndexPath(forRow: 2, inSection: 1)
    
      if let cell = tableView.cellForRowAtIndexPath(indexPathDateRow) {
        cell.detailTextLabel!.textColor = UIColor(white: 0, alpha: 0.5)
      }
    
      tableView.beginUpdates()
      tableView.reloadRowsAtIndexPaths([indexPathDateRow], withRowAnimation: .None)
      tableView.deleteRowsAtIndexPaths([indexPathDatePicker], withRowAnimation: .Fade)
      tableView.endUpdates()
    }
  }
  
  func dateChanged(datePicker: UIDatePicker) {
    dueDate = datePicker.date
    updateDueDateLabel()
  }
}
