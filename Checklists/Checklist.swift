//
//  Checklist.swift
//  Checklists
//
//  Created by Pascal Huynh on 10/11/14.
//  Copyright (c) 2014 Beyowi. All rights reserved.
//

import UIKit

class Checklist: NSObject, NSCoding {
  var name = ""
  var items = [ChecklistItem]()
  var iconName: String
  
  init(name: String, iconName: String) {
    self.name = name
    self.iconName = iconName
    super.init()
  }

  convenience init(name: String) {
    self.init(name: name, iconName: "Folder")
  }
  
  // For loading
  required init(coder aDecoder: NSCoder) {
    name = aDecoder.decodeObjectForKey("Name") as String
    items = aDecoder.decodeObjectForKey("Items") as [ChecklistItem]
    iconName = aDecoder.decodeObjectForKey("IconName") as String
    super.init()
    sortItems()
  }
  
  // For saving
  func encodeWithCoder(aCoder: NSCoder) {
    aCoder.encodeObject(name, forKey: "Name")
    aCoder.encodeObject(items, forKey: "Items")
    aCoder.encodeObject(iconName, forKey: "IconName")
  }
  
  func countUncheckItems() -> Int {
    var count = 0
    for item in items {
      if !item.checked {
        count++
      }
    }
    return count
  }
  
  func sortItems() {
    items.sort({ item1, item2 in return
      item1.dueDate.compare(item2.dueDate) == NSComparisonResult.OrderedAscending })
  }
}
