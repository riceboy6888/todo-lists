//
//  AppDelegate.swift
//  Checklists
//
//  Created by Pascal Huynh on 25/10/14.
//  Copyright (c) 2014 Beyowi. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?
  let dataModel = DataModel()

  func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
    // We need to unwrap with '!' because window is an optional and we didn't unwrap with if let syntax
    // Also we are sure that window is never nil so we use '!' instead of '?'
    let navigationController = window!.rootViewController as UINavigationController
    // We don't want to use 'topViewController' bc it will return the view that is currently displayed.
    // If the user is looking at a todo list instead, the app will crash
    let controller = navigationController.viewControllers[0] as AllListsViewController
    controller.dataModel = dataModel
    
//    // Ask alert permission for local notification
//    let notificationSettings = UIUserNotificationSettings(forTypes: .Alert | .Sound, categories: nil)
//    UIApplication.sharedApplication().registerUserNotificationSettings(notificationSettings)
    
//    // Create and schedule a local notification that will be fired after 10 seconds
//    let date = NSDate(timeIntervalSinceNow: 10)
//    let localNotification = UILocalNotification()
//    localNotification.fireDate = date
//    localNotification.timeZone = NSTimeZone.defaultTimeZone()
//    localNotification.alertBody = "I am a local notification!"
//    localNotification.soundName = UILocalNotificationDefaultSoundName
//    
//    UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
    
    return true
  }

  func applicationWillResignActive(application: UIApplication) {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
  }

  func applicationDidEnterBackground(application: UIApplication) {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    saveData()
  }

  func applicationWillEnterForeground(application: UIApplication) {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
  }

  func applicationDidBecomeActive(application: UIApplication) {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
  }

  func applicationWillTerminate(application: UIApplication) {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    saveData()
  }

  func application(application: UIApplication, didReceiveLocalNotification notification: UILocalNotification) {
    println("didReceiveLocalNotification \(notification)")
  }
  
  func saveData() {
    dataModel.saveChecklists()
    println("Data saved")
  }
}
