//
//  ViewController.swift
//  Checklists
//
//  Created by Pascal Huynh on 25/10/14.
//  Copyright (c) 2014 Beyowi. All rights reserved.
//

import UIKit

class ChecklistViewController: UITableViewController, ItemDetailViewControllerDelegate {

  // nil is not an allowed value for variables in Swift but by using ! we ovverride that
  // indeed we temporally allow a nil value for 'checklist' because it is only initialized in viewDidLoad
  // After Tap > Create View > init (here checklist should have a value but we allow a temporary nil) > prepareForSegue
  var checklist: Checklist!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    tableView.rowHeight = 44
    // Change the title in the navigation bar
    title = checklist.name
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return checklist.items.count
  }
  
  override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("ChecklistItem") as UITableViewCell
    let item = checklist.items[indexPath.row]

    configureTextForCell(cell, withChecklistItem: item)
    configureCheckmarkForCell(cell, withChecklistItem: item)
    
    return cell
  }
  
  override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    // Toggle checkmark on clicked cell
    if let cell = tableView.cellForRowAtIndexPath(indexPath) {
      let item = checklist.items[indexPath.row]
      item.toggleChecked()
      
      configureCheckmarkForCell(cell, withChecklistItem: item)
    }
    tableView.deselectRowAtIndexPath(indexPath, animated: true)
  }

  // Delete rows
  override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    // Remove the item from the data model
    checklist.items.removeAtIndex(indexPath.row)
    
    // Remove the item from the table view
    let indexPaths = [indexPath]
    tableView.deleteRowsAtIndexPaths(indexPaths, withRowAnimation: .Automatic)
  }
  
  func configureTextForCell(cell: UITableViewCell, withChecklistItem item: ChecklistItem) {
    let label = cell.viewWithTag(1000) as UILabel
    let detailLabel = cell.viewWithTag(1100) as UILabel
    label.text = item.text
    detailLabel.text = item.dueDateToString()
    //label.text = "\(item.itemID): \(item.text)"
  }
  
  func configureCheckmarkForCell(cell: UITableViewCell, withChecklistItem item: ChecklistItem) {
    let label = cell.viewWithTag(1001) as UILabel
    label.textColor = view.tintColor
    
    if item.checked {
      label.text = "√"
    } else {
      label.text = ""
    }
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Because there may be more than one segue per view controller, it’s a good idea to give each one a unique identifier
    if segue.identifier == "AddItem" {
      // The new view controller can be found in segue.destinationViewController. 
      // The storyboard shows that the segue does not go directly to ItemDetailViewController but to the navigation controller that embeds it.
      let navigationController = segue.destinationViewController as UINavigationController
      
      // To find the ItemDetailViewController, you can look at the navigation controller’s topViewController property. 
      // This property refers to the screen that is currently active inside the navigation controller.
      let controller = navigationController.topViewController as ItemDetailViewController
      
      // Once you have a reference to the ItemDetailViewController object, you set its delegate property to self and the connection is complete.
      controller.delegate = self
    } else if segue.identifier == "EditItem" {
      let navigationController = segue.destinationViewController as UINavigationController
      let controller = navigationController.topViewController as ItemDetailViewController
      controller.delegate = self
      
      // Here the sender is the tapped cell
      // In fact sender contains a reference to the control that triggered the segue
      if let indexPath = tableView.indexPathForCell(sender as UITableViewCell) {
        controller.itemToEdit = checklist.items[indexPath.row]
      }
    }
  }
  
  func itemDetailViewControllerDidCancel(controller: ItemDetailViewController) {
    dismissViewControllerAnimated(true, completion: nil)
  }
  
  func itemDetailViewController(controller: ItemDetailViewController, didFinishAddingItem item: ChecklistItem) {
    let newRowIndex = checklist.items.count
    checklist.items.append(item)
    checklist.sortItems()
    
    // Set the index path of the new item
    //let indexPath = NSIndexPath(forRow: newRowIndex, inSection: 0)
    // Create an array with indexPath included
    //let indexPaths = [indexPath]
    
    //tableView.insertRowsAtIndexPaths(indexPaths, withRowAnimation: .Automatic)
    tableView.reloadData()
    dismissViewControllerAnimated(true, completion: nil)
  }
  
  func itemDetailViewController(controller: ItemDetailViewController, didFinishEditingItem item: ChecklistItem) {
    //if let index = find(checklist.items, item) {
    //  let indexPath = NSIndexPath(forRow: index, inSection: 0)
    //  if let cell = tableView.cellForRowAtIndexPath(indexPath) {
    //    configureTextForCell(cell, withChecklistItem: item)
    //  }
    //}
    checklist.sortItems()
    tableView.reloadData()
    dismissViewControllerAnimated(true, completion: nil)
  }
}

