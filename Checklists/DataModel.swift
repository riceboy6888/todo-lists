//
//  DataModel.swift
//  Checklists
//
//  Created by Pascal Huynh on 12/11/14.
//  Copyright (c) 2014 Beyowi. All rights reserved.
//

import Foundation

class DataModel {
  var lists = [Checklist]()
  var indexOfSelectedChecklist: Int {
    get {
      return NSUserDefaults.standardUserDefaults().integerForKey("ChecklistIndex")
    }
    set {
      NSUserDefaults.standardUserDefaults().setInteger(newValue, forKey: "ChecklistIndex")
    }
  }
  
  init() {
    println("### Data Model Initialization")
    println(dataFilePath())
    loadChecklists()
    registerDefaults()
    handleFirstTime()
    // Don't need to call super.init() because there is no superclass
  }
  
  // Method that gets the current 'ChecklistItemID" value, adds one to it and writes it back to NSUserDefaults
  class func nextChecklistItemID() -> Int {
    let userDefaults = NSUserDefaults.standardUserDefaults()
    let itemID = userDefaults.integerForKey("ChecklistItemID")
    userDefaults.setInteger(itemID + 1, forKey: "ChecklistItemID")
    userDefaults.synchronize()
    return itemID
  }
  
  // Save and Load features method
  func documentsDirectory() -> String {
    let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true) as [String]
    return paths[0]
  }
  
  func dataFilePath() -> String {
    return documentsDirectory().stringByAppendingPathComponent("Checklists.plist")
  }
  
  func saveChecklists() {
    let data = NSMutableData()
    let archiver = NSKeyedArchiver(forWritingWithMutableData: data)
    archiver.encodeObject(lists, forKey: "Checklists")
    archiver.finishEncoding()
    data.writeToFile(dataFilePath(), atomically: true)
  }
  
  func loadChecklists() {
    let path = dataFilePath()
    if NSFileManager.defaultManager().fileExistsAtPath(path) {
      if let data = NSData(contentsOfFile: path) {
        let unarchiver = NSKeyedUnarchiver(forReadingWithData: data)
        lists = unarchiver.decodeObjectForKey("Checklists") as [Checklist]
        unarchiver.finishDecoding()
        sortChecklists()
      }
    }
  }

  // Set -1 as a default for "ChecklistIndex" instead of 0 by defaults (which can be an existing entry in the array)
  func registerDefaults() {
    let dictionary = [ "ChecklistIndex": -1,
                       "FirstTime": true,
                       "ChecklistItemID": 0 ]
    NSUserDefaults.standardUserDefaults().registerDefaults(dictionary)
  }
  
  func handleFirstTime() {
    let userDefaults = NSUserDefaults.standardUserDefaults()
    let firstTime = userDefaults.boolForKey("FirstTime")
    if firstTime {
      let checklist = Checklist(name: "List")
      lists.append(checklist)
      indexOfSelectedChecklist = 0
      userDefaults.setBool(false, forKey: "FirstTime")
    }
  }
  
  func sortChecklists() {
    lists.sort({ checklist1, checklist2 in return
    checklist1.name.localizedStandardCompare(checklist2.name) == NSComparisonResult.OrderedAscending })
  }
}