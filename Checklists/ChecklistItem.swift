//
//  ChecklistItem.swift
//  Checklists
//
//  Created by Pascal Huynh on 28/10/14.
//  Copyright (c) 2014 Beyowi. All rights reserved.
//

import Foundation
import UIKit

class ChecklistItem: NSObject, NSCoding {
  var text = ""
  var checked = false
  var dueDate = NSDate()
  var shouldRemind = false
  var itemID: Int

  init(text: String, checked: Bool) {
    self.text = text
    self.checked = checked
    self.itemID = DataModel.nextChecklistItemID()
    super.init()
  }

  convenience init(text: String) {
    self.init(text: text, checked: false)
  }
  
  override init() {
    self.itemID = DataModel.nextChecklistItemID()
    super.init()
  }
  
  // Remove local notification when deleting
  deinit {
    let existingNotification = notificationForThisItem()
    if let notification = existingNotification {
      //println("Removing existing notification \(notification)")
      UIApplication.sharedApplication().cancelLocalNotification(notification)
    }
  }
  
  func toggleChecked() {
    checked = !checked
  }
  
  // Save
  func encodeWithCoder(aCoder: NSCoder) {
    aCoder.encodeObject(text, forKey: "Text")
    aCoder.encodeBool(checked, forKey: "Checked")
    aCoder.encodeObject(dueDate, forKey: "DueDate")
    aCoder.encodeBool(shouldRemind, forKey: "ShouldRemind")
    aCoder.encodeInteger(itemID, forKey: "ItemID")
  }
  
  // Load
  required init(coder aDecoder: NSCoder) {
    text = aDecoder.decodeObjectForKey("Text") as String
    checked = aDecoder.decodeBoolForKey("Checked")
    dueDate = aDecoder.decodeObjectForKey("DueDate") as NSDate
    shouldRemind = aDecoder.decodeBoolForKey("ShouldRemind")
    itemID = aDecoder.decodeIntegerForKey("ItemID")
    super.init()
  }
  
  func scheduleNotification() {
    // Cancel local notification
    let existingNotification = notificationForThisItem()
    if let notification = existingNotification {
      //println("Found an existing notification \(notification)")
      UIApplication.sharedApplication().cancelLocalNotification(notification)
    }
    
    // This compare the due Date with the current time
    // With NSComparisonResult.OrderedAscending, dueDate comes before the current date and time
    // With NSComparisonResult.OrderedSame, dueDate = current date and time
    // With NSComparisonResult.OrderedDescending, dueDate comes after the current date and time
    if shouldRemind && dueDate.compare(NSDate()) != NSComparisonResult.OrderedAscending {
      let localNotification = UILocalNotification()
      localNotification.fireDate = dueDate
      localNotification.timeZone = NSTimeZone.defaultTimeZone()
      localNotification.alertBody = text
      localNotification.soundName = UILocalNotificationDefaultSoundName

      // Add a userInfo dictionary with the item’s ID as the only contents.
      // That is how you’ll be able to find this notification later in case you need to cancel it.
      localNotification.userInfo = ["ItemID": itemID]
      
      UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
      
      //println("Scheduled notification \(localNotification) for itemID \(itemID)")
    }
  }
  
  func notificationForThisItem() -> UILocalNotification? {
    let allNotifications = UIApplication.sharedApplication().scheduledLocalNotifications as [UILocalNotification]
    for notification in allNotifications {
      if let number = notification.userInfo?["ItemID"] as? NSNumber {
        if number.integerValue == itemID {
          return notification
        }
      }
    }
    return nil
  }
  
  // Convert due date to text
  func dueDateToString() -> String {
    let formatter = NSDateFormatter()
    formatter.dateStyle = .MediumStyle
    formatter.timeStyle = .ShortStyle
    return formatter.stringFromDate(dueDate)
  }
}





